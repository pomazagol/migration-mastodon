  Mastodon - Changer d'instance 

Mastodon - Changer d'instance
=============================

_11 novembre 2022 / November 11, 2022 - P.-O. Mazagol_

[**Français**](#French)/[**English**](#english)

  
\---  

**Procédure pour déplacer son compte d'une instance Mastodon à une autre (_a priori_ il est préférable que les serveurs des deux instances soient à jour)**

Sur la nouvelle instance :

1\. Créer un compte _e.g._ compte@nouvelle.instance

2\. Créer un alias pour l'ancien compte  
_Paramètres_ -> _Compte_ -> _Déplacement depuis un compte différent_ -> _Créer un alias de compte_ : compte@ancienne.instance

Sur l'ancienne instance :

3\. Exporter ce qui peut l'être  
_Paramètres_ -> _Import et export_ -> _Export de données_ -> Télécharger les CSV proposés selon les besoins (On peut encore le faire une fois la redirection faite, mais mieux vaut être prudent)

4\. Rediriger le compte de l'ancienne instance vers la nouvelle  
_Paramètres_ -> _Compte_ -> _Déménager vers un compte différent_ : compte@nouvelle.instance | mot de passe (de l'ancienne instance)

Sur la nouvelle instance :

5\. Attendre que le serveur de la nouvelle instance rapatrie les abonnées (délais variable)

6\. En attendant, importer les CVS dans le comptre sur la nouvelle instance  
_Paramètres_ -> _Import et export_ -> _Import de données_

7\. Attendre que le serveur rapatrie vos abonnements et autres (délais variable)

8\. Configurer avatar, bio et régler les paramètres selon les envies

Remarque : Les pouets/toots restent sur l'ancienne instance. Votre compte n'est pas supprimé sur l'ancienne instance. 

\---

**Method to move your account from a Mastodon instance to another one ( it seems better that servers of both instances are up to date)**

On the new instance:

1\. Create an account e.g. account@new.instance

2\. Create an alias for your old account  
_Preferences_ -> _Account_ -> _Account settings_ -> _Moving from a different account_ -> _Create account alias_: account@former.instance

On the old instance:

3\. Export what you want  
_Preferences_ -> _Import and export_ -> _Date Export_ -> Download CSVs as needed (You can do that later, but it is better to be careful)

4\. Redirect account from old instance to new one  
_Preferences_ -> _Account_ -> _Move to a different account_: account@new.instance | password (from old instance)

On the new instance:

5\. Wait for the new instance server to retrieve your followers (this can be take some time)

6\. In the meantime, import the CVS in the account on the new instance  
_Preferences_ -> _Import and export_ -> _Import_

7\. Wait for server to retrieve your follows (as exemple) (this can be take some time)

8\. Add your avatar, bio and set preferences

Note: Toots remain on the old instance. Your account is not deleted on the old instance.
